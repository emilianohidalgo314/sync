from tkinter import *

class Calculadora:

    def __init__(self, master):
        self.master = master
        master.title("Calculadora")

        #Vars
        self.tipoIP = StringVar()
        self.mascRed = StringVar()
        self.tipoIP.set("Tipo de IP: ")
        self.mascRed.set("Máscara de Subred: ")
        self.selec = IntVar()

        #Label
        self.etTipoIP = Label(master, textvariable = self.tipoIP)
        self.etmascRed = Label(master, textvariable = self.mascRed)

        #Entry
        self.inIP = Entry(master, text = "0.0.0.0")
        self.inNum = Entry(master, width=6)

        #Botones
        self.btEnter = Button(master, text="Calcular", command=self.obtenerCaractIP)
        self.bHosts = Radiobutton(master, text="Número de Hosts",variable=self.selec, value=0)
        self.bSubredes = Radiobutton(master, text="Número de Subredes",variable=self.selec, value=1)

	    #Lista y scroll
        self.scrollbarH = Scrollbar(master)
        self.scrollbarS = Scrollbar(master)
        self.lHosts = Listbox(master, selectmode=SINGLE)
        self.lSubredes = Listbox(master, selectmode=SINGLE)

        #Binds
        self.lHosts.bind('<<ListboxSelect>>', self.hostSelec)
        self.lSubredes.bind('<<ListboxSelect>>', self.subredSelec)

        #Pack
        self.inIP.grid(row=0, column=0)
        self.inNum.grid(sticky="W",row=1, column=1)
        self.etTipoIP.grid(row=0, column=2)
        self.bHosts.grid(sticky="W", row=1, column=0)
        self.bSubredes.grid(sticky="W", row=2, column=0)
        self.etmascRed.grid(row=1, column=2)
        self.lHosts.grid(sticky="E",row=9, column=0)
        self.lSubredes.grid(sticky="E", row=9, column=1)
        self.btEnter.grid(row=10, column=1)
        self.lHosts.config(yscrollcommand=self.scrollbarH.set)
        self.lSubredes.config(yscrollcommand=self.scrollbarS.set)

    def obtenerCaractIP(self):
        ip = self.inIP.get()
        ipSplit = ip.split('.')
        try:
            oct = int(ipSplit[0])
        except(ValueError):
            oct = 0
        if (oct in range(1,128)):
            self.tipoIP.set("Tipo de IP: A")
        elif oct in range(128,192):
            self.tipoIP.set("Tipo de IP: B")
        elif oct in range(192,223):
            self.tipoIP.set("Tipo de IP: C")
        else:
            self.tipoIP.set("IP NO VALIDA")

        self.mascRed.set("Máscara se Subred: " +'.'.join(["255" if "0" not in x else "0" for x in ipSplit]))

    def hostSelec(self):
        print(":p")

    def subredSelec(self):
        print(":p")


root = Tk()
my_gui = Calculadora(root)
root.mainloop()

