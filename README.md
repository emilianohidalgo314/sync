LibSync. V.05

Programa para sincronizar bibliotecas musicales entre dispostivos, escrito en Python3.
    **USO EN WINDOWS**
    
    Funcionalidades:
        *Guardado de playlists
        *Cargado de playlists
        *Generar playlist apartir de carpeta destino 
        
    Formatos soportados:
        '*.mp3','*.wav','*.aac','*.ogg','*wma','*.flac','*.alac','*.aiff','*.pcm'
        
    ToDo:
        *Mensajes ayuda/error
        *Mayor rango de opciones para controlar sync:
            *Seleccionar todo
            *Deseleccionar todo
    Done:
        *Mostrar peso de canciones seleccionadas
        *Mostrar num de canciones seleccionadas
            
        